import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Banco, Bancos } from '../models/Bancos';

@Injectable({
  providedIn: 'root'
})
export class BancosService {

  constructor(private http: HttpClient) { }

  dataGetBancos() : Promise<Bancos> {
    const apiUrl = `assets/data/db.json`;
    return this.http.get<Bancos>(apiUrl).toPromise();
  }

}
