import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BancosComponent } from './bancos.component';
import { HttpClientModule } from '@angular/common/http';
import { ModalSucursalesModule } from 'src/app/shared/modales/modal-sucursales/modal-sucursales.module';
import { BsModalService } from 'ngx-bootstrap/modal';



@NgModule({
  declarations: [
    BancosComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    ModalSucursalesModule
  ],
  exports: [
    BancosComponent
  ],
  providers: [BsModalService]
})
export class BancosModule { }
