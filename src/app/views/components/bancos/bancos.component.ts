import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Banco, Bancos, Sucursales } from './models/Bancos';
import { BancosService } from './services/diccionario.service';

@Component({
  selector: 'app-bancos',
  templateUrl: './bancos.component.html',
  styleUrls: ['./bancos.component.scss']
})
export class BancosComponent implements OnInit {

  /* Modales */
  configBackdrop = {
    animated: true,
    keyboard: true,
    backdrop: true,
    class: 'modal-lg',
    ignoreBackdropClick: true,
  };

  listadoBancos!: any;
  listadoSucursales: Sucursales[] = [];

  constructor( private bancosService: BancosService, private bsModalService: BsModalService) { }

  ngOnInit(): void {
    this.inicializaValores();
  }

  inicializaValores() {
    this.getBancos();
  }

  openModalVerSucursales(verSucursales: TemplateRef<any>, sucursales: Sucursales[]) {
    this.bsModalService.show(verSucursales, this.configBackdrop);
    this.listadoSucursales = sucursales;
  }

  getBancos() {

    this.bancosService.dataGetBancos().then((banco: Bancos) => {
      this.listadoBancos = banco.bancos;
    })
  }

}
